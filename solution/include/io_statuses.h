//
// Created by vlad on 11/18/22.
//

#ifndef IMAGE_TRANSFORMER_IO_STATUSES_H
#define IMAGE_TRANSFORMER_IO_STATUSES_H

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE = 1,
    READ_INVALID_BITS = 2,
    READ_INVALID_HEADER = 3
    /* коды других ошибок  */
};

#endif //IMAGE_TRANSFORMER_IO_STATUSES_H
