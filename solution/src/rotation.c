//
// Created by vlad on 11/18/22.
//

#include "image.h"
#include "rotation.h"
#include <stddef.h>


struct image rotate( struct image const source ) {
    const struct image rotated = create_image(source.height, source.width);
    if (rotated.data == NULL)
        return rotated;
    struct pixel* current_pixel = rotated.data;
    for (size_t i = 0; i < source.width; i++) {
        for (int64_t j = (int64_t)((source.height-1)*source.width); j >= 0; j -= (int64_t)source.width) {
            *current_pixel = source.data[j + i];
            current_pixel++;
        }
    }
    return rotated;
}
