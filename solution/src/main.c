#include <stdio.h>

#include "utils.h"
#include "bmp.h"
#include "image.h"
#include "rotation.h"

int main( int argc, char** argv ) {

    char *src_path, *out_path;
    if (!parse_params(argc, argv, &src_path, &out_path)) {
        printf("Wrong args!");
        return 1;
    }

    struct image img = {0};
    read_from_file(&img, src_path, from_bmp);

    struct image rotated = rotate(img);
    write_to_file(&rotated, out_path, to_bmp);

    clear_image_memory(&img);
    clear_image_memory(&rotated);
    return 0;
}
