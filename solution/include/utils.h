//
// Created by vlad on 11/6/22.
//

#ifndef IMAGE_TRANSFORMER_UTILS_H
#define IMAGE_TRANSFORMER_UTILS_H

#include "image.h"
#include "io_statuses.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

void read_from_file(struct image* img, const char* path, enum read_status (reader)(FILE* in, struct image* img));
void write_to_file(struct image* img, const char* path, enum write_status (writer)(FILE* in, const struct image* img));

bool parse_params(int argc, char** argv, char** src_path, char** out_path);
void throw_exception(const char* message, uint64_t code);


#endif //IMAGE_TRANSFORMER_UTILS_H
