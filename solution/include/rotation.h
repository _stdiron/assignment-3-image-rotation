//
// Created by vlad on 11/18/22.
//

#ifndef IMAGE_TRANSFORMER_ROTATION_H
#define IMAGE_TRANSFORMER_ROTATION_H

struct image rotate( struct image const source );

#endif //IMAGE_TRANSFORMER_ROTATION_H
