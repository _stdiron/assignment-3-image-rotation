//
// Created by vlad on 11/18/22.
//

#ifndef IMAGE_TRANSFORMER_FILE_H
#define IMAGE_TRANSFORMER_FILE_H

#include "stdio.h"

enum open_file_status {
    OPEN_SUCCESS = 1,
    OPEN_ERROR = 0
};

enum close_file_status {
    CLOSE_SUCCESS = 1,
    CLOSE_ERROR = 0
};

enum open_file_status open_file(FILE** file, const char* mode, const char* path);
enum close_file_status close_file(FILE** file);

#endif //IMAGE_TRANSFORMER_FILE_H
